#include<stdio.h>
int main()
{
	int r,c,x,y;
	printf("Enter no. of rows and columns of 2D matrix\n");
	scanf("%d %d",&r,&c);
	int ar[r][c];
	for(x=0;x<r;x++)
	{
	   printf("Enter data of row %d\n",x+1);
	   for(y=0;y<c;y++)
		{
			scanf("%d",&ar[x][y]);
		}
	}
	printf("Transpose of 2D matrix is\n");
	for(x=0;x<c;x++)
	{
		for(y=0;y<r;y++)
		{
			printf("%d",ar[y][x]);
		}
		printf("\n");
	}
	return 0;
}
