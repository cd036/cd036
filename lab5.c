#include<stdio.h>
int main()
{
    int n,x;
    printf("Enter the no. of elements in array \n");
    scanf("%d",&n);
    float a[n],min,max,t;
    int min_pos=0,max_pos=0;
    printf("Enter array elements \n");
    for(x=0;x<n;x++)
    scanf("%f",&a[x]);
    printf("Before interchange \n");
	for(x=0;x<n;x++)
	printf("%f ",a[x]);
    for(x=0;x<n;x++)
    {
        if(x==0)
        {
            min=a[x];
            max=a[x];
        }
        else
        {
            if(a[x]>=max)
            {
                max_pos=x;
                max=a[x];
            }
                if(a[x]<=min)
                {
                    min_pos=x;
                    min=a[x];
                }
        }
    }
    t=a[max_pos];
    a[max_pos]=a[min_pos];
    a[min_pos]=t;
    printf("\nAfter interchange \n");
    for(x=0;x<n;x++)
    printf("%f ",a[x]);
    return 0;
}