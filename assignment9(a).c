#include<stdio.h>
int main()
{
	int r,c,x,y;
	printf("Enter no. of rows and columns of 2D array\n");
	scanf("%d %d",&r,&c);
	int ar[r][c];
	for(x=0;x<r;x++)
	{
	   printf("Enter data of row %d\n",x+1);
	   for(y=0;y<c;y++)
		{
			scanf("%d",&ar[x][y]);
		}
	}
	printf("2D array is\n");
	for(x=0;x<r;x++)
	{
		for(y=0;y<c;y++)
		{
			printf("%d",ar[x][y]);
		}
		printf("\n");
	}
	return 0;
}
